package com.zsisistemas.zsicashflow.view.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.ActivityDespesaBinding;
import com.zsisistemas.zsicashflow.databinding.AdicionarInvestimentoDialogoBinding;
import com.zsisistemas.zsicashflow.models.InvestimentoModel;
import com.zsisistemas.zsicashflow.utils.Util;
import com.zsisistemas.zsicashflow.view.adapters.InvestimentoAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class DespesaActivity extends AppCompatActivity {

    private ActivityDespesaBinding binding;
    private List<InvestimentoModel> despesas;
    private InvestimentoAdapter despesaAdapter;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityDespesaBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());
        setSupportActionBar(binding.despesaActivityToolBar);

        despesas = new ArrayList<>(despesasBuilder());
        despesaAdapter = new InvestimentoAdapter(getApplicationContext(), despesas);
        binding.despesaActivityRvDespesa.setAdapter(despesaAdapter);
        binding.despesaActivityRvDespesa.addItemDecoration(new DividerItemDecoration(
                binding.despesaActivityRvDespesa.getContext(), DividerItemDecoration.VERTICAL));

        somarTotalDespesa();
    }

    private void somarTotalDespesa() {
        Double soma = 0.00;

        for(InvestimentoModel dm : despesas) {
            soma += dm.getValor();
        }

        binding.despesaActivityTvValorTotal.setText(Util.formatarCurrency(soma));
    }

    public List<InvestimentoModel> despesasBuilder() {
        return Arrays.asList(
                new InvestimentoModel("3\nJan", "Água", 800.00),
                new InvestimentoModel("28\nFev", "Aluguel", 2000.00),
                new InvestimentoModel("15\nJun", "Brinde aos funcionários", 120.00),
                new InvestimentoModel("8\nOut", "Contador", 150.00),
                new InvestimentoModel("10\nNov", "Gás", 350.00),
                new InvestimentoModel("22\nDez", "Internet", 180.00),
                new InvestimentoModel("25\nSet", "Luz", 700.00),
                new InvestimentoModel("11\nAbr", "Manutenção do computador", 100.00),
                new InvestimentoModel("19\nJul", "Materiais do escritório", 80.00),
                new InvestimentoModel("7\nAgo", "Produtos de limpeza", 140.00));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_investimento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.investimento_mnAdicioanr) {
            mostrarDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("SetTextI18n")
    public void mostrarDialog() {
        AdicionarInvestimentoDialogoBinding binding;
        binding = AdicionarInvestimentoDialogoBinding.inflate(getLayoutInflater());

        AlertDialog adicionarDespesaDialogAlert = new AlertDialog.Builder(this).create();
        adicionarDespesaDialogAlert.setView(binding.getRoot());
        adicionarDespesaDialogAlert.show();

        binding.adicionarInvestimentoTvLabel.setText("Adicionar nova despesa");

        binding.adicionarInvestimentoBtAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String descricao = Objects.requireNonNull(binding.adicionarInvestimentoItDescricao.getEditText()).getText().toString().trim();
                String data = Objects.requireNonNull(binding.adicionarInvestimentoItData.getEditText()).getText().toString().trim();
                String valor = Objects.requireNonNull(binding.adicionarInvestimentoItValor.getEditText()).getText().toString().trim();

                if (!descricao.isEmpty() && !data.isEmpty() && !valor.isEmpty()) {
                    insertItem(new InvestimentoModel(data.replace(" ", " \n"), descricao, Double.parseDouble(valor)));
                    adicionarDespesaDialogAlert.cancel();
                } else {
                    Toast.makeText(DespesaActivity.this, "Preencha todos os campos!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void insertItem(InvestimentoModel despesa) {
        despesas.add(0, despesa);
        despesaAdapter.notifyItemInserted(0);
        binding.despesaActivityRvDespesa.scrollToPosition(0);
        somarTotalDespesa();
    }
}