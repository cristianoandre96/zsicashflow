package com.zsisistemas.zsicashflow.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.EntradaSaidaItemBinding;
import com.zsisistemas.zsicashflow.models.EntradaSaidaModel;
import com.zsisistemas.zsicashflow.utils.Util;

import java.util.List;

public class EntradaSaidaAdapter extends RecyclerView.Adapter<EntradaSaidaAdapter.InvestimentoHolder> {

    private final Context ctx;
    private final List<EntradaSaidaModel> entradas_saidas;

    public EntradaSaidaAdapter(Context ctx, List<EntradaSaidaModel> entradas_saidas) {
        this.ctx = ctx;
        this.entradas_saidas = entradas_saidas;
    }

    @NonNull
    @Override
    public InvestimentoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        EntradaSaidaItemBinding binding = EntradaSaidaItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new InvestimentoHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull InvestimentoHolder holder, int position) {
        EntradaSaidaModel entradaSaida = entradas_saidas.get(position);

        holder.binding.rvEntradaSaidaTvData.setText(entradaSaida.getData());
        holder.binding.rvEntradaSaidaTvTotalEntradaValor.setText(Util.formatarCurrency(entradaSaida.getTotalEntrada()));
        holder.binding.rvEntradaSaidaTvTotalSaidaValor.setText(Util.formatarCurrency(entradaSaida.getTotalSaida()));
        holder.binding.rvrvEntradaSaidaTvSubTotalValor.setText(Util.formatarCurrency(entradaSaida.getSubTotal()));

        holder.binding.rvrvEntradaSaidaTvSubTotalValor.setTextColor(entradaSaida.getTotalEntrada() >= entradaSaida.getTotalSaida()
        ? ctx.getResources().getColor(R.color.green_A700) : ctx.getResources().getColor(R.color.red));
    }

    @Override
    public int getItemCount() {
        return entradas_saidas != null ? entradas_saidas.size() : 0;
    }

    public void removeItem(int index) {
        entradas_saidas.remove(index);
        notifyItemRemoved(index);
    }

    public static class InvestimentoHolder extends RecyclerView.ViewHolder {

        EntradaSaidaItemBinding binding;

        public InvestimentoHolder(EntradaSaidaItemBinding itemBinding) {
            super(itemBinding.getRoot());

            binding = itemBinding;
        }
    }
}
