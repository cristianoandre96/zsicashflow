package com.zsisistemas.zsicashflow.view.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rodarSplashScreen();
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        abrirHomeActivity();
    }

    public void rodarSplashScreen() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        setTheme(R.style.Theme_ZsiCashFlow);
    }

    public void abrirHomeActivity() {
        startActivity(new Intent(this, HomeActivity.class));
        finish();
    }
}