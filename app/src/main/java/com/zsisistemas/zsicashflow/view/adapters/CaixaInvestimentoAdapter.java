package com.zsisistemas.zsicashflow.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.CaixaItemBinding;
import com.zsisistemas.zsicashflow.models.InvestimentoModel;
import com.zsisistemas.zsicashflow.utils.Util;

import java.util.List;

public class CaixaInvestimentoAdapter extends RecyclerView.Adapter<CaixaInvestimentoAdapter.InvestimentoHolder> {

    private final Context ctx;
    private final List<InvestimentoModel> investimentos;

    public CaixaInvestimentoAdapter(Context ctx, List<InvestimentoModel> investimentos) {
        this.ctx = ctx;
        this.investimentos = investimentos;
    }

    @NonNull
    @Override
    public InvestimentoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CaixaItemBinding binding = CaixaItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new InvestimentoHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull InvestimentoHolder holder, int position) {
        InvestimentoModel investimento = investimentos.get(position);

        holder.binding.rvCaixaTvDescricao.setText(investimento.getDescricao());
        holder.binding.rvCaixaTvValor.setText(Util.formatarCurrency(investimento.getValor()));

        holder.binding.rvCaixaTvDescricao.setTextColor(investimento.getValor() > 0.00
                ? ctx.getResources().getColor(R.color.black)
                : ctx.getResources().getColor(R.color.gray));

        holder.binding.rvCaixaTvValor.setTextColor(investimento.getValor() > 0.00
                ? ctx.getResources().getColor(R.color.black)
                : ctx.getResources().getColor(R.color.gray));

        holder.binding.caixaItemCardView.setCardElevation(investimento.getValor() > 0.00 ? 6f : 0f);
    }

    @Override
    public int getItemCount() {
        return investimentos != null ? investimentos.size() : 0;
    }

    public void insertItem(InvestimentoModel investimento) {
        investimentos.add(0, investimento);
        notifyItemInserted(0);
    }

    public static class InvestimentoHolder extends RecyclerView.ViewHolder {

        CaixaItemBinding binding;

        public InvestimentoHolder(CaixaItemBinding itemBinding) {
            super(itemBinding.getRoot());

            binding = itemBinding;
        }
    }
}
