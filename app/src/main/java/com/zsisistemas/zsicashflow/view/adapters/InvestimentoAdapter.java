package com.zsisistemas.zsicashflow.view.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsicashflow.databinding.InvestimentoItemBinding;
import com.zsisistemas.zsicashflow.models.InvestimentoModel;
import com.zsisistemas.zsicashflow.utils.Util;

import java.util.List;

public class InvestimentoAdapter extends RecyclerView.Adapter<InvestimentoAdapter.InvestimentoHolder> {

    private Context ctx;
    private List<InvestimentoModel> investimentos;

    public InvestimentoAdapter(Context ctx, List<InvestimentoModel> investimentos) {
        this.ctx = ctx;
        this.investimentos = investimentos;
    }

    @NonNull
    @Override
    public InvestimentoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        InvestimentoItemBinding binding = InvestimentoItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new InvestimentoHolder(binding);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull InvestimentoHolder holder, int position) {
        InvestimentoModel investimento = investimentos.get(position);

        holder.binding.rvInvestimentoTvData.setText(investimento.getData());
        holder.binding.rvInvestimentoTvDescricao.setText(investimento.getDescricao());
        holder.binding.rvInvestimentoTvValor.setText(Util.formatarCurrency(investimento.getValor()));
    }

    @Override
    public int getItemCount() {
        return investimentos != null ? investimentos.size() : 0;
    }

    public static class InvestimentoHolder extends RecyclerView.ViewHolder{

        InvestimentoItemBinding binding;

        public InvestimentoHolder(@NonNull InvestimentoItemBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;
        }
    }
}
