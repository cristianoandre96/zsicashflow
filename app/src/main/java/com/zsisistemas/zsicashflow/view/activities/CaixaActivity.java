package com.zsisistemas.zsicashflow.view.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.ActivityCaixaBinding;
import com.zsisistemas.zsicashflow.databinding.AdicionarInvestimentoDialogoBinding;
import com.zsisistemas.zsicashflow.databinding.CaixaItemDialogoBinding;
import com.zsisistemas.zsicashflow.models.ConfiguracaoModel;
import com.zsisistemas.zsicashflow.models.InvestimentoModel;
import com.zsisistemas.zsicashflow.models.UsuarioModel;
import com.zsisistemas.zsicashflow.utils.Util;
import com.zsisistemas.zsicashflow.view.adapters.CaixaDespesaAdapter;
import com.zsisistemas.zsicashflow.view.adapters.CaixaFormaPgtoAdapter;
import com.zsisistemas.zsicashflow.view.adapters.CaixaInvestimentoAdapter;
import com.zsisistemas.zsicashflow.view.adapters.CaixaValeAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CaixaActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private ActivityCaixaBinding binding;
    private List<ConfiguracaoModel> formas_pgto;
    private CaixaFormaPgtoAdapter caixaFormaPgtoAdapter;
    private ConfiguracaoActivity configuracaoActivity;
    private List<InvestimentoModel> investimentos, despesas, vales;
    private CaixaInvestimentoAdapter caixaInvestimentoAdapter;
    private CaixaDespesaAdapter caixaDespesaAdapter;
    private CaixaValeAdapter caixaValeAdapter;
    private List<UsuarioModel> usuarios;
    private UsuarioActivity usuarioActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityCaixaBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());

        configuracaoActivity = new ConfiguracaoActivity();
        formas_pgto = new ArrayList<>(configuracaoActivity.configuracoesBuilder());
        caixaFormaPgtoAdapter = new CaixaFormaPgtoAdapter(getApplicationContext(), formas_pgto);
        binding.caixaActivityRvReceita.setAdapter(caixaFormaPgtoAdapter);

        investimentos = new ArrayList<>();
        caixaInvestimentoAdapter = new CaixaInvestimentoAdapter(getApplicationContext(), investimentos);
        binding.caixaActivityRvInvestimento.setAdapter(caixaInvestimentoAdapter);

        despesas = new ArrayList<>();
        caixaDespesaAdapter = new CaixaDespesaAdapter(getApplicationContext(), despesas);
        binding.caixaActivityRvDespesa.setAdapter(caixaDespesaAdapter);

        vales = new ArrayList<>();
        caixaValeAdapter = new CaixaValeAdapter(getApplicationContext(), vales);
        binding.caixaActivityRvVale.setAdapter(caixaValeAdapter);

        somarTotais();

        usuarioActivity = new UsuarioActivity();
        usuarios = new ArrayList<>(usuarioActivity.usuariosBuilder());
        ArrayAdapter<UsuarioModel> spinnerUsuarioAdapter = new ArrayAdapter<UsuarioModel>(this, android.R.layout.simple_spinner_item, usuarios);
        spinnerUsuarioAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.caixaActivityTsFuncionario.setAdapter(spinnerUsuarioAdapter);

        binding.caixaActivityTsFuncionario.setOnItemSelectedListener(this);

        binding.caixaActivityRvReceita.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), binding.caixaActivityRvReceita, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                ConfiguracaoModel forma_pgto = formas_pgto.get(position);
                binding.caixaActivityRvReceita.requestFocus();
                CaixaItemDialogoBinding dialogBinding = CaixaItemDialogoBinding.inflate(getLayoutInflater());
                AlertDialog atualizarFormaPgtoDialogAlert = new AlertDialog.Builder(CaixaActivity.this).create();
                atualizarFormaPgtoDialogAlert.setView(dialogBinding.getRoot());
                atualizarFormaPgtoDialogAlert.show();

                dialogBinding.caixaItemTvDescricao.setText(forma_pgto.getDescricao());
                Objects.requireNonNull(dialogBinding.caixaItemItValor.getEditText()).setText(String.valueOf(forma_pgto.getValor()));

                dialogBinding.caixaItemBtAdicionar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String campoValor = Objects.requireNonNull(dialogBinding.caixaItemItValor.getEditText()).getText().toString().trim();
                        String campoDescricao = dialogBinding.caixaItemTvDescricao.getText().toString().trim();

                        if (!campoValor.isEmpty()) {
                            Double novoValor = Double.parseDouble(campoValor);
                            caixaFormaPgtoAdapter.updateItem(position, new ConfiguracaoModel(campoDescricao, novoValor));
                        }

                        somarTotalEntrada();
                        atualizarFormaPgtoDialogAlert.cancel();
                    }
                });
            }

            @Override
            public void onLongItemClick(View view, int position) {
            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        }));

        binding.caixaActivityTvAdicionarInvestimento.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.caixaActivityRvInvestimento.requestFocus();
                AdicionarInvestimentoDialogoBinding dialogBinding = AdicionarInvestimentoDialogoBinding.inflate(getLayoutInflater());
                AlertDialog adicionarInvestimentoDialogAlert = new AlertDialog.Builder(CaixaActivity.this).create();
                adicionarInvestimentoDialogAlert.setView(dialogBinding.getRoot());
                adicionarInvestimentoDialogAlert.show();

                dialogBinding.adicionarInvestimentoBtAdicionar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String campoDescricao = Objects.requireNonNull(dialogBinding.adicionarInvestimentoItDescricao.getEditText()).getText().toString().trim();
                        String campoData = Objects.requireNonNull(dialogBinding.adicionarInvestimentoItData.getEditText()).getText().toString().trim();
                        String campoValor = Objects.requireNonNull(dialogBinding.adicionarInvestimentoItValor.getEditText()).getText().toString().trim();

                        if (!campoDescricao.isEmpty() && !campoData.isEmpty() && !campoValor.isEmpty()) {
                            caixaInvestimentoAdapter.insertItem(new InvestimentoModel(campoData.replace(" ", " \n"), campoDescricao, Double.parseDouble(campoValor)));
                            adicionarInvestimentoDialogAlert.cancel();
                            binding.caixaActivityRvInvestimento.scrollToPosition(0);
                            somarTotalInvestimentoSaida();
                        } else {
                            Toast.makeText(CaixaActivity.this, "Preencha todos os campos!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        binding.caixaActivityTvAdicionarDespesa.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                binding.caixaActivityRvDespesa.requestFocus();
                AdicionarInvestimentoDialogoBinding dialogBinding = AdicionarInvestimentoDialogoBinding.inflate(getLayoutInflater());
                AlertDialog adicionarDespesaDialogAlert = new AlertDialog.Builder(CaixaActivity.this).create();
                adicionarDespesaDialogAlert.setView(dialogBinding.getRoot());
                adicionarDespesaDialogAlert.show();

                dialogBinding.adicionarInvestimentoTvLabel.setText("Adicionar nova despesa");

                dialogBinding.adicionarInvestimentoBtAdicionar.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(View v) {
                        String campoDescricao = Objects.requireNonNull(dialogBinding.adicionarInvestimentoItDescricao.getEditText()).getText().toString().trim();
                        String campoData = Objects.requireNonNull(dialogBinding.adicionarInvestimentoItData.getEditText()).getText().toString().trim();
                        String campoValor = Objects.requireNonNull(dialogBinding.adicionarInvestimentoItValor.getEditText()).getText().toString().trim();

                        if (!campoDescricao.isEmpty() && !campoData.isEmpty() && !campoValor.isEmpty()) {
                            caixaDespesaAdapter.insertItem(new InvestimentoModel(campoData.replace(" ", " \n"), campoDescricao, Double.parseDouble(campoValor)));
                            adicionarDespesaDialogAlert.cancel();
                            binding.caixaActivityRvDespesa.scrollToPosition(0);
                            somarTotalDespesaSaida();
                        } else {
                            Toast.makeText(CaixaActivity.this, "Preencha todos os campos!", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        });

        binding.caixaActivityTvAdicionarVale.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                binding.caixaActivityRvVale.requestFocus();
                CaixaItemDialogoBinding dialogBinding = CaixaItemDialogoBinding.inflate(getLayoutInflater());
                AlertDialog adicionarValeDialogAlert = new AlertDialog.Builder(CaixaActivity.this).create();
                adicionarValeDialogAlert.setView(dialogBinding.getRoot());
                adicionarValeDialogAlert.show();

                dialogBinding.caixaItemTvDescricao.setText("Adicionar novo vale");

                dialogBinding.caixaItemBtAdicionar.setOnClickListener(new View.OnClickListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onClick(View v) {
                        String campoValor = Objects.requireNonNull(dialogBinding.caixaItemItValor.getEditText()).getText().toString().trim();

                        if (!campoValor.isEmpty()) {
                            caixaValeAdapter.insertItem(new InvestimentoModel("", "", Double.parseDouble(campoValor)));
                            adicionarValeDialogAlert.cancel();
                            binding.caixaActivityRvVale.scrollToPosition(0);
                            somarTotalValeSaida();
                        }
                    }
                });
            }
        });
    }

    public void torcarVisibilidadeListas(boolean visivel) {
        binding.caixaActivityConstraintLayoutReceita.setVisibility(visivel ? View.GONE : View.VISIBLE);
        binding.caixaActivityConstraintLayoutInvestimento.setVisibility(visivel ? View.GONE : View.VISIBLE);
        binding.caixaActivityConstraintLayoutDespesa.setVisibility(visivel ? View.GONE : View.VISIBLE);
        binding.caixaActivityConstraintLayoutVale.setVisibility(visivel ? View.GONE : View.VISIBLE);
    }

    public void somarTotais() {
        somarTotalEntrada();
        somarTotalInvestimentoSaida();
        somarTotalDespesaSaida();
        somarTotalValeSaida();
    }

    public void somarTotalEntrada() {
        Double soma = 0.00;
        if (formas_pgto!=null) for(ConfiguracaoModel fpm : formas_pgto) {
            soma += fpm.getValor();
        }
        binding.caixaActivityTvTotalEntradaValor.setText(Util.formatarCurrency(soma));
        trocarCoresTextosTotalEntrada(soma);
    }

    public void somarTotalInvestimentoSaida() {
        Double soma = 0.00;
        if (investimentos!=null) for(InvestimentoModel im : investimentos) {
            soma += im.getValor();
        }
        binding.caixaActivityTvTotalSaidaInvestimentoValor.setText(Util.formatarCurrency(soma));
        trocarCoresTextosTotalInvestimentoSaida(soma);
    }

    public void somarTotalDespesaSaida() {
        Double soma = 0.00;
        if (despesas!=null) for(InvestimentoModel im : despesas) {
            soma += im.getValor();
        }
        binding.caixaActivityTvTotalSaidaDespesaValor.setText(Util.formatarCurrency(soma));
        trocarCoresTextosTotalDespesaSaida(soma);
    }

    public void somarTotalValeSaida() {
        Double soma = 0.00;
        if (vales!=null) for(InvestimentoModel im : vales) {
            soma += im.getValor();
        }
        binding.caixaActivityTvTotalSaidaValeValor.setText(Util.formatarCurrency(soma));
        trocarCoresTextosTotalValeSaida(soma);
    }

    public void trocarCoresTextosTotalEntrada(Double totalEntrada) {
        binding.caixaActivityTvTotalEntradaValor.setTextColor(totalEntrada > 0.00
                ? getResources().getColor(R.color.green_A700)
                : getResources().getColor(R.color.gray));
        binding.caixaActivityTvTotalEntrada.setTextColor(totalEntrada > 0.00
                ? getResources().getColor(R.color.green_A700)
                : getResources().getColor(R.color.gray));
    }

    public void trocarCoresTextosTotalInvestimentoSaida(Double totalEntrada) {
        binding.caixaActivityTvTotalSaidaInvestimentoValor.setTextColor(totalEntrada > 0.00
                ? getResources().getColor(android.R.color.holo_red_light)
                : getResources().getColor(R.color.gray));
        binding.caixaActivityTvTotalSaidaInvestimento.setTextColor(totalEntrada > 0.00
                ? getResources().getColor(android.R.color.holo_red_light)
                : getResources().getColor(R.color.gray));
    }

    public void trocarCoresTextosTotalDespesaSaida(Double totalEntrada) {
        binding.caixaActivityTvTotalSaidaDespesaValor.setTextColor(totalEntrada > 0.00
                ? getResources().getColor(android.R.color.holo_red_light)
                : getResources().getColor(R.color.gray));
        binding.caixaActivityTvTotalSaidaDespesa.setTextColor(totalEntrada > 0.00
                ? getResources().getColor(android.R.color.holo_red_light)
                : getResources().getColor(R.color.gray));
    }

    public void trocarCoresTextosTotalValeSaida(Double totalEntrada) {
        binding.caixaActivityTvTotalSaidaValeValor.setTextColor(totalEntrada > 0.00
                ? getResources().getColor(android.R.color.holo_red_light)
                : getResources().getColor(R.color.gray));
        binding.caixaActivityTvTotalSaidaVale.setTextColor(totalEntrada > 0.00
                ? getResources().getColor(android.R.color.holo_red_light)
                : getResources().getColor(R.color.gray));
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        torcarVisibilidadeListas(false);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }
}