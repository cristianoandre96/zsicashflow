package com.zsisistemas.zsicashflow.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.CaixaItemBinding;
import com.zsisistemas.zsicashflow.models.ConfiguracaoModel;
import com.zsisistemas.zsicashflow.utils.Util;

import java.util.List;

public class CaixaFormaPgtoAdapter extends RecyclerView.Adapter<CaixaFormaPgtoAdapter.FormaPgtoHolder> {

    private Context ctx;
    private List<ConfiguracaoModel> formas_pgto;

    public CaixaFormaPgtoAdapter(Context ctx, List<ConfiguracaoModel> formas_pgto) {
        this.ctx = ctx;
        this.formas_pgto = formas_pgto;
    }

    @NonNull
    @Override
    public FormaPgtoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CaixaItemBinding binding = CaixaItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new FormaPgtoHolder(binding, ctx);
    }

    @Override
    public void onBindViewHolder(@NonNull FormaPgtoHolder holder, int position) {
        ConfiguracaoModel forma_pgto = formas_pgto.get(position);

        holder.binding.rvCaixaTvDescricao.setText(forma_pgto.getDescricao());
        holder.binding.rvCaixaTvValor.setText(Util.formatarCurrency(forma_pgto.getValor()));

        holder.binding.rvCaixaTvDescricao.setTextColor(forma_pgto.getValor() > 0.00
                ? ctx.getResources().getColor(R.color.black)
                : ctx.getResources().getColor(R.color.gray));

        holder.binding.rvCaixaTvValor.setTextColor(forma_pgto.getValor() > 0.00
                ? ctx.getResources().getColor(R.color.black)
                : ctx.getResources().getColor(R.color.gray));

        holder.binding.caixaItemCardView.setCardElevation(forma_pgto.getValor() > 0.00 ? 6f : 0f);
    }

    @Override
    public int getItemCount() {
        return formas_pgto != null ? formas_pgto.size() : 0;
    }

    public void updateItem(int index, ConfiguracaoModel obj) {
        ConfiguracaoModel newObj = new ConfiguracaoModel(obj.getDescricao(), obj.getValor());
        formas_pgto.set(index, newObj);
        notifyItemChanged(index);
    }

    public static class FormaPgtoHolder extends RecyclerView.ViewHolder {

        CaixaItemBinding binding;

        public FormaPgtoHolder(CaixaItemBinding itemBinding, Context ctx) {
            super(itemBinding.getRoot());

            binding = itemBinding;
        }
    }
}
