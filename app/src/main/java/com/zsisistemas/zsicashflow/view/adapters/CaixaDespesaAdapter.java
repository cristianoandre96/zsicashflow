package com.zsisistemas.zsicashflow.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.CaixaItemBinding;
import com.zsisistemas.zsicashflow.models.InvestimentoModel;
import com.zsisistemas.zsicashflow.utils.Util;

import java.util.List;

public class CaixaDespesaAdapter extends RecyclerView.Adapter<CaixaDespesaAdapter.DespesaHolder> {

    private Context ctx;
    private List<InvestimentoModel> despesas;

    public CaixaDespesaAdapter(Context ctx, List<InvestimentoModel> despesas) {
        this.ctx = ctx;
        this.despesas = despesas;
    }

    @NonNull
    @Override
    public DespesaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CaixaItemBinding binding = CaixaItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new DespesaHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull DespesaHolder holder, int position) {
        InvestimentoModel despesa = despesas.get(position);

        holder.binding.rvCaixaTvDescricao.setText(despesa.getDescricao());
        holder.binding.rvCaixaTvValor.setText(Util.formatarCurrency(despesa.getValor()));

        holder.binding.rvCaixaTvDescricao.setTextColor(despesa.getValor() > 0.00
                ? ctx.getResources().getColor(R.color.black)
                : ctx.getResources().getColor(R.color.gray));

        holder.binding.rvCaixaTvValor.setTextColor(despesa.getValor() > 0.00
                ? ctx.getResources().getColor(R.color.black)
                : ctx.getResources().getColor(R.color.gray));

        holder.binding.caixaItemCardView.setCardElevation(despesa.getValor() > 0.00 ? 6f : 0f);
    }

    @Override
    public int getItemCount() {
        return despesas != null ? despesas.size() : 0;
    }

    public void insertItem(InvestimentoModel investimento) {
        despesas.add(0, investimento);
        notifyItemInserted(0);
    }

    public static class DespesaHolder extends RecyclerView.ViewHolder {

        CaixaItemBinding binding;

        public DespesaHolder(CaixaItemBinding itemBinding) {
            super(itemBinding.getRoot());

            binding = itemBinding;
        }
    }
}
