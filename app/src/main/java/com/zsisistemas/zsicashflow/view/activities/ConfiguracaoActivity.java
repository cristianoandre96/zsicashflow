package com.zsisistemas.zsicashflow.view.activities;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.zsisistemas.zsicashflow.databinding.ActivityConfiguracaoBinding;
import com.zsisistemas.zsicashflow.models.ConfiguracaoModel;
import com.zsisistemas.zsicashflow.view.adapters.ConfiguracaoAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ConfiguracaoActivity extends AppCompatActivity {

    private ActivityConfiguracaoBinding binding;
    private List<ConfiguracaoModel> configuracoes;
    private ConfiguracaoAdapter configuracaoAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityConfiguracaoBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());

        configuracoes = new ArrayList<>(configuracoesBuilder());
        configuracaoAdapter = new ConfiguracaoAdapter(getApplicationContext(), configuracoes);
        binding.configuracaoActivityRvConfiguracao.setAdapter(configuracaoAdapter);
    }

    public List<ConfiguracaoModel> configuracoesBuilder() {
        return Arrays.asList(
                new ConfiguracaoModel("Dinheiro", 0.00),
                new ConfiguracaoModel("Cartão de crédito", 0.00),
                new ConfiguracaoModel("Cartão de débito", 0.00),
                new ConfiguracaoModel("Pix", 0.00));
    }
}