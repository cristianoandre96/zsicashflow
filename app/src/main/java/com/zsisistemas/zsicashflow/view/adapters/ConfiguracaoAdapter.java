package com.zsisistemas.zsicashflow.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsicashflow.databinding.ConfiguracaoItemBinding;
import com.zsisistemas.zsicashflow.models.ConfiguracaoModel;

import net.yslibrary.android.keyboardvisibilityevent.util.UIUtil;

import java.util.List;

public class ConfiguracaoAdapter extends RecyclerView.Adapter<ConfiguracaoAdapter.ConfiguracaoHolder> {

    private Context ctx;
    private List<ConfiguracaoModel> configuracoes;
    private int atualPosicaoExpandida = -1;
    private int ultimaPosicaoExpandida = -1;

    public ConfiguracaoAdapter(Context ctx, List<ConfiguracaoModel> configuracoes) {
        this.ctx = ctx;
        this.configuracoes = configuracoes;
    }

    @NonNull
    @Override
    public ConfiguracaoAdapter.ConfiguracaoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConfiguracaoItemBinding binding = ConfiguracaoItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new ConfiguracaoHolder(binding, ctx);
    }

    @Override
    public void onBindViewHolder(@NonNull ConfiguracaoAdapter.ConfiguracaoHolder holder, int position) {
        ConfiguracaoModel configuracao = configuracoes.get(position);
        holder.binding.rvConfiguracaoEtDescricao.setText(configuracao.getDescricao());

        holder.binding.rvConfiguracaoTvAdicionar.setVisibility(position == 0 ? View.VISIBLE : View.GONE);

        boolean isExpandido = position == atualPosicaoExpandida;
        holder.binding.rvConfiguracaoConstraintLayout.setVisibility(isExpandido ? View.VISIBLE : View.GONE);
        holder.binding.rvConfiguracaoConstraintLayout.setActivated(isExpandido);
        holder.binding.rvConfiguracaoEtDescricao.setFocusableInTouchMode(isExpandido);
        holder.binding.rvConfiguracaoEtDescricao.setFocusable(isExpandido);
        holder.binding.rvConfiguracaoEtDescricao.setSingleLine(!isExpandido);
        holder.binding.rvConfiguracaoEtDescricao.requestFocus();

        if (isExpandido) ultimaPosicaoExpandida = position;

        holder.binding.rvConfiguracaoEtDescricao.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    UIUtil.hideKeyboard(ctx, v);
                }
            }
        });

        holder.binding.rvConfiguracaoEtDescricao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!holder.binding.rvConfiguracaoEtDescricao.hasFocus()) {
                    atualPosicaoExpandida = position;
                    notifyItemChanged(ultimaPosicaoExpandida);
                    notifyItemChanged(position);
                }
            }
        });

        holder.binding.rvConfiguracaoTvAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                insertItem(new ConfiguracaoModel());
                holder.binding.rvConfiguracaoEtDescricao.performClick();
            }
        });

        holder.binding.rvConfiguracaoBtSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String descricao = holder.binding.rvConfiguracaoEtDescricao.getText().toString().trim();
                if (descricao.isEmpty()){
                    holder.binding.rvConfiguracaoTvCancelar.performClick();
                } else {
                    updateItem(position, new ConfiguracaoModel(descricao, 0.00));
                    holder.binding.rvConfiguracaoTvCancelar.performClick();
                }
            }
        });

        holder.binding.rvConfiguracaoTvCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                atualPosicaoExpandida = isExpandido ? -1 : position;
                notifyItemChanged(ultimaPosicaoExpandida);
                notifyItemChanged(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return configuracoes != null ? configuracoes.size() : 0;
    }

    public void insertItem(ConfiguracaoModel configuracao) {
        configuracoes.add(0,configuracao);
        notifyItemRangeChanged(0, getItemCount());
    }

    public void updateItem(int index, ConfiguracaoModel obj) {
        ConfiguracaoModel newObj = new ConfiguracaoModel(obj.getDescricao(), obj.getValor());
        configuracoes.set(index, newObj);
        notifyItemChanged(index);
    }

    public static class ConfiguracaoHolder extends RecyclerView.ViewHolder{

        ConfiguracaoItemBinding binding;

        public ConfiguracaoHolder(@NonNull ConfiguracaoItemBinding itemBinding, Context ctx){
            super(itemBinding.getRoot());
            binding = itemBinding;
        }
    }
}
