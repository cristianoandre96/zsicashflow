package com.zsisistemas.zsicashflow.view.adapters;

import android.content.Context;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.ColorInt;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsicashflow.databinding.UsuarioItemBinding;
import com.zsisistemas.zsicashflow.models.UsuarioModel;
import com.zsisistemas.zsicashflow.utils.Util;

import java.util.List;

public class UsuarioAdapter extends RecyclerView.Adapter<UsuarioAdapter.UsuarioViewHolder>{

    private final Context ctx;
    private final List<UsuarioModel> usuarios;

    public UsuarioAdapter(Context ctx, List<UsuarioModel> usuarios) {
        this.ctx = ctx;
        this.usuarios = usuarios;
    }

    @NonNull
    @Override
    public UsuarioAdapter.UsuarioViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        UsuarioItemBinding binding = UsuarioItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new UsuarioViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull UsuarioAdapter.UsuarioViewHolder holder, int position) {
        UsuarioModel usuario = usuarios.get(position);

        holder.binding.rvUsuarioTvIcon.setText(String.valueOf(usuario.getNome().charAt(0)));
        holder.binding.rvUsuarioTvIcon.setBackground(formarOvalBackground_TvIcon(Util.formarCores(usuario), holder.binding.rvUsuarioTvIcon));
        holder.binding.rvUsuarioTvNome.setText(usuario.getNome());
        holder.binding.rvUsuarioTvPerfil.setText(usuario.getPerfil());
    }

    @Override
    public int getItemCount() {
        return usuarios != null ? usuarios.size() : 0;
    }

    public static class UsuarioViewHolder extends RecyclerView.ViewHolder {

        UsuarioItemBinding binding;

        public UsuarioViewHolder(@NonNull UsuarioItemBinding itemBinding) {
            super(itemBinding.getRoot());
            binding = itemBinding;
        }
    }

    private static ShapeDrawable formarOvalBackground_TvIcon(@ColorInt int color, View view) {
        ShapeDrawable shapeDrawable = new ShapeDrawable(new OvalShape());
        shapeDrawable.setIntrinsicHeight(view.getHeight());
        shapeDrawable.setIntrinsicWidth(view.getWidth());
        shapeDrawable.getPaint().setColor(color);
        return shapeDrawable;
    }
}
