package com.zsisistemas.zsicashflow.view.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;

import androidx.appcompat.app.AppCompatActivity;

import com.zsisistemas.zsicashflow.databinding.ActivityUsuarioBinding;
import com.zsisistemas.zsicashflow.models.UsuarioModel;
import com.zsisistemas.zsicashflow.view.adapters.UsuarioAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class UsuarioActivity extends AppCompatActivity {

    private ActivityUsuarioBinding binding;
    private List<UsuarioModel> usuarios;
    private UsuarioAdapter usuarioAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityUsuarioBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());

        usuarios = new ArrayList<>(usuariosBuilder());
        usuarioAdapter = new UsuarioAdapter(getApplicationContext(), usuarios);

        binding.usuarioActivityRvUsuario.setAdapter(usuarioAdapter);

        binding.usuarioActivityRvUsuario.addOnItemTouchListener(
                new RecyclerItemClickListener(
                        getApplicationContext(),
                        binding.usuarioActivityRvUsuario,
                        new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                UsuarioModel usuario = usuarios.get(position);
                                startActivity(new Intent(getApplicationContext(), DetalheUsuarioActivity.class)
                                        .putExtra("usuario", usuario));
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {
                            }

                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            }
                        }
                )
        );

        binding.usuarioActivityFabCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CadastroUsuarioActivity.class));
            }
        });
    }

    public List<UsuarioModel> usuariosBuilder() {
        return Arrays.asList(
                new UsuarioModel("Joule","Admin"),
                new UsuarioModel("Kensei", "Funcionário"),
                new UsuarioModel("Alpha", "Funcionário"),
                new UsuarioModel("Lance", "Funcionário"),
                new UsuarioModel("Lorelai", "Funcionário"),
                new UsuarioModel("Magnus", "Funcionário"),
                new UsuarioModel("Ozo", "Funcionário"),
                new UsuarioModel("Skaarf", "Funcionário"),
                new UsuarioModel("Rona", "Funcionário"),
                new UsuarioModel("Yates", "Funcionário"));
    }
}