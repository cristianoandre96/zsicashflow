package com.zsisistemas.zsicashflow.view.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.ActivityDetalheUsuarioBinding;
import com.zsisistemas.zsicashflow.models.UsuarioModel;
import com.zsisistemas.zsicashflow.utils.Util;

public class DetalheUsuarioActivity extends AppCompatActivity {

    private ActivityDetalheUsuarioBinding binding;
    private UsuarioModel usuario;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityDetalheUsuarioBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());
        setSupportActionBar(binding.detalheUsuarioToolBar);

        usuario = getIntent().getParcelableExtra("usuario");

        binding.detalheUsuarioTvUsuario.setBackgroundColor(Util.formarCores(usuario));
        binding.detalheUsuarioToolBar.setBackgroundColor(Util.formarCores(usuario));
        binding.detalheUsuarioTvUsuario.setText(usuario.getNome());
        binding.detalheUsuarioTvPerfil.setText(usuario.getPerfil());

        Util.trocarCorStatusBar(this, Util.escurecerCoresFormadas(Util.formarCores(usuario), getApplicationContext()));

        binding.detalheUsuarioTvAdicionarVale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Clicou em: Adicionar vale", Toast.LENGTH_SHORT).show();
            }
        });

        binding.detalheUsuarioTvValeTotal.setText("R$100.00");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detalhe_usuario, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.detalhe_usuario_mnEditar:
                startActivity(new Intent(getApplicationContext(), CadastroUsuarioActivity.class)
                        .putExtra("usuario", usuario));
            break;
            case R.id.detalhe_usuario_mnExcluir:
                Toast.makeText(this, "Clicou em: Excluir", Toast.LENGTH_SHORT).show();
            break;
            default:
        }
        return super.onOptionsItemSelected(item);
    }
}