package com.zsisistemas.zsicashflow.view.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.ActivityCadastroUsuarioBinding;
import com.zsisistemas.zsicashflow.models.UsuarioModel;
import com.zsisistemas.zsicashflow.utils.Util;

import java.util.Objects;

public class CadastroUsuarioActivity extends AppCompatActivity {

    private ActivityCadastroUsuarioBinding binding;
    private UsuarioModel usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityCadastroUsuarioBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());

        usuario = getIntent().getParcelableExtra("usuario");

        if(usuario != null) {
//            Objeto para edição
            setTitle("Editar usuário");
            binding.cadastroUsuarioTvIcon.setBackgroundColor(Util.formarCores(usuario));
            Objects.requireNonNull(binding.cadastroUsuarioItNome.getEditText()).setText(usuario.getNome());
            Objects.requireNonNull(binding.cadastroUsuarioItPerfil.getEditText()).setText(usuario.getPerfil());
        }
        /*else {
//            Objeto para criação
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cadastro_usuario, menu);
        return true;
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.cadastro_usuario_mn_confirmar) {
            Toast.makeText(getApplicationContext(),
                    usuario != null ? "Clicou em: Confirmar edição" : "Clicou em: Confirmar cadastro",
                    Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }
}