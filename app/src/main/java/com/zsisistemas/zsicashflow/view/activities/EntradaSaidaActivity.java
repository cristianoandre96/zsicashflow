package com.zsisistemas.zsicashflow.view.activities;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.ActivityEntradaSaidaBinding;
import com.zsisistemas.zsicashflow.models.EntradaSaidaModel;
import com.zsisistemas.zsicashflow.utils.Util;
import com.zsisistemas.zsicashflow.view.adapters.EntradaSaidaAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EntradaSaidaActivity extends AppCompatActivity {

    private ActivityEntradaSaidaBinding binding;
    private List<EntradaSaidaModel> entradas_saidas;
    private EntradaSaidaAdapter entradaSaidaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityEntradaSaidaBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());
        setSupportActionBar(binding.entradaSaidaToolBar);

        entradas_saidas = new ArrayList<>(entradas_saidasBuilder());
        entradaSaidaAdapter = new EntradaSaidaAdapter(getApplicationContext(), entradas_saidas);
        binding.entradaSaidaRvEntradaSaida.setAdapter(entradaSaidaAdapter);
        binding.entradaSaidaRvEntradaSaida.addItemDecoration(new DividerItemDecoration(
                binding.entradaSaidaRvEntradaSaida.getContext(), DividerItemDecoration.VERTICAL));

        somarTotais();

        binding.entradaSaidaRvEntradaSaida.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), binding.entradaSaidaRvEntradaSaida, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
            }

            @Override
            public void onLongItemClick(View view, int position) {
                mostrarPopupMenu(view, position);
            }

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_entrada_saida, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.entrada_saida_mnFiltros) {
            Toast.makeText(getApplicationContext(), "Clicou em: Filtros", Toast.LENGTH_SHORT).show();
        }
        return super.onOptionsItemSelected(item);
    }

    public void somarTotais() {
        somarEntradaGeral();
        somarSaidaGeral();
        somarSubtotalGeral();
    }

    public void trocarPaletaCores(Double subTotal) {
        binding.entradaSaidaToolBar.setBackgroundColor(subTotal < 0
                ? getResources().getColor(R.color.red)
                : getResources().getColor(R.color.green_A700));
        binding.entradaSaidaTvSubtotalGeralValor.setBackground(subTotal < 0
                ? ContextCompat.getDrawable(getApplicationContext(), R.drawable.red_gradiente_bg)
                : ContextCompat.getDrawable(getApplicationContext(), R.drawable.green_gradiente_bg));
        Util.trocarCorStatusBar(this, subTotal < 0
                ? getResources().getColor(R.color.darker_red)
                : getResources().getColor(R.color.darker_green_A700));
    }

    public void somarEntradaGeral() {
        Double soma = 0.00;

        for(EntradaSaidaModel esm : entradas_saidas) {
            soma += esm.getTotalEntrada();
        }

        binding.entradaSaidaTvEntradaGeralValor.setText(Util.formatarCurrency(soma));
    }

    public void somarSaidaGeral() {
        Double soma = 0.00;

        for(EntradaSaidaModel esm : entradas_saidas) {
            soma += esm.getTotalSaida();
        }

        binding.entradaSaidaTvSaidaGeralValor.setText(Util.formatarCurrency(soma));
    }

    public void somarSubtotalGeral() {
        Double soma = 0.00;

        for(EntradaSaidaModel esm : entradas_saidas) {
            soma += esm.getSubTotal();
        }

        trocarPaletaCores(soma);

        binding.entradaSaidaTvSubtotalGeralValor.setText(Util.formatarCurrency(soma));
    }

    public List<EntradaSaidaModel> entradas_saidasBuilder() {
        return Arrays.asList(
                new EntradaSaidaModel("2\nJan", 100.00, 1000.00, -900.00),
                new EntradaSaidaModel("3\nFev", 200.00, 900.00, -700.00),
                new EntradaSaidaModel("5\nMar", 300.00, 800.00, -500.00),
                new EntradaSaidaModel("27\nAbr", 400.00, 700.00, -300.00),
                new EntradaSaidaModel("31\nMai", 500.00, 600.00, -100.00),
                new EntradaSaidaModel("14\nJun", 600.00, 500.00, 100.00),
                new EntradaSaidaModel("25\nJul", 700.00, 400.00, 300.00),
                new EntradaSaidaModel("26\nOut", 800.00, 300.00, 500.00),
                new EntradaSaidaModel("10\nNov", 900.00, 200.00, 700.00),
                new EntradaSaidaModel("9\nDez", 1000.00, 100.00, 900.00));
    }

    public void mostrarPopupMenu(View v, int index) {
        PopupMenu popupMenu = new PopupMenu(v.getContext(), v, Gravity.END, R.attr.actionOverflowMenuStyle, 0);
        popupMenu.getMenuInflater().inflate(R.menu.popup_menu_entrada_saida_item, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @SuppressLint("NonConstantResourceId")
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.entrada_saida_item_popup_mnDetalhe:
                        Toast.makeText(getApplicationContext(), "Clicou em: Detalhes", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.entrada_saida_item_popup_mnExcluir:
                        entradaSaidaAdapter.removeItem(index);
                        somarTotais();
                        break;
                    default:
                }
                return true;
            }
        });
        popupMenu.show();
    }
}