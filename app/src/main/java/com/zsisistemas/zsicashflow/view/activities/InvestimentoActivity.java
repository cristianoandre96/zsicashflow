package com.zsisistemas.zsicashflow.view.activities;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.ActivityInvestimentoBinding;
import com.zsisistemas.zsicashflow.databinding.AdicionarInvestimentoDialogoBinding;
import com.zsisistemas.zsicashflow.models.InvestimentoModel;
import com.zsisistemas.zsicashflow.utils.Util;
import com.zsisistemas.zsicashflow.view.adapters.InvestimentoAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class InvestimentoActivity extends AppCompatActivity {

    private ActivityInvestimentoBinding binding;
    private List<InvestimentoModel> investimentos;
    private InvestimentoAdapter investimentoAdapter;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        binding = ActivityInvestimentoBinding.inflate(getLayoutInflater());
        super.onCreate(savedInstanceState);
        setContentView(binding.getRoot());
        setSupportActionBar(binding.investimentoActivityToolBar);

        investimentos = new ArrayList<>(investimentosBuilder());
        investimentoAdapter = new InvestimentoAdapter(getApplicationContext(), investimentos);
        binding.investimentoActivityRvInvestimento.setAdapter(investimentoAdapter);
        binding.investimentoActivityRvInvestimento.addItemDecoration(new DividerItemDecoration(
                binding.investimentoActivityRvInvestimento.getContext(), DividerItemDecoration.VERTICAL));

        somarTotalInvestido();
    }

    private void somarTotalInvestido() {
        Double soma = 0.00;

        for(InvestimentoModel im : investimentos) {
            soma += im.getValor();
        }

        binding.investimentoActivityTvValorTotal.setText(Util.formatarCurrency(soma));
    }

    public List<InvestimentoModel> investimentosBuilder() {
        return Arrays.asList(
                new InvestimentoModel("3\nJan", "Renda Fixa", 15000.00),
                new InvestimentoModel("28\nFev", "Líquidez imediata", 15000.00),
                new InvestimentoModel("15\nJun", "Ações Brasil", 20000.00),
                new InvestimentoModel("8\nOut", "Fundos imobiliários", 10000.00),
                new InvestimentoModel("10\nNov", "Multimercado", 5000.00),
                new InvestimentoModel("22\nDez", "Ouro", 8000.00),
                new InvestimentoModel("25\nSet", "Estruturadas", 5000.00),
                new InvestimentoModel("11\nAbr", "Moedas", 7000.00),
                new InvestimentoModel("19\nJul", "Ações EUA", 15000.00),
                new InvestimentoModel("7\nAgo", "Imóveis", 30000.00));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_investimento, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.investimento_mnAdicioanr) {
            mostrarDialog();
        }
        return super.onOptionsItemSelected(item);
    }

    public void mostrarDialog() {
        AdicionarInvestimentoDialogoBinding binding = AdicionarInvestimentoDialogoBinding.inflate(getLayoutInflater());

        AlertDialog adicionarInvestimentoDialogAlert = new AlertDialog.Builder(this).create();
        adicionarInvestimentoDialogAlert.setView(binding.getRoot());
        adicionarInvestimentoDialogAlert.show();

        binding.adicionarInvestimentoBtAdicionar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String descricao = Objects.requireNonNull(binding.adicionarInvestimentoItDescricao.getEditText()).getText().toString().trim();
                String data = Objects.requireNonNull(binding.adicionarInvestimentoItData.getEditText()).getText().toString().trim();
                String valor = Objects.requireNonNull(binding.adicionarInvestimentoItValor.getEditText()).getText().toString().trim();

                if (!descricao.isEmpty() && !data.isEmpty() && !valor.isEmpty()) {
                    insertItem(new InvestimentoModel(data.replace(" ", " \n"), descricao, Double.parseDouble(valor)));
                    adicionarInvestimentoDialogAlert.cancel();
                } else {
                    Toast.makeText(InvestimentoActivity.this, "Preencha todos os campos!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void insertItem(InvestimentoModel investimento) {
        investimentos.add(0, investimento);
        investimentoAdapter.notifyItemInserted(0);
        binding.investimentoActivityRvInvestimento.scrollToPosition(0);
        somarTotalInvestido();
    }
}