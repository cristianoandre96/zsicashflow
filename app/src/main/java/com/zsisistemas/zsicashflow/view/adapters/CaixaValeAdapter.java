package com.zsisistemas.zsicashflow.view.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.databinding.CaixaItemBinding;
import com.zsisistemas.zsicashflow.models.InvestimentoModel;
import com.zsisistemas.zsicashflow.utils.Util;

import java.util.List;

public class CaixaValeAdapter extends RecyclerView.Adapter<CaixaValeAdapter.CaixaHolder> {

    private Context ctx;
    private List<InvestimentoModel> vales;

    public CaixaValeAdapter(Context ctx, List<InvestimentoModel> vales) {
        this.ctx = ctx;
        this.vales = vales;
    }

    @NonNull
    @Override
    public CaixaHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CaixaItemBinding binding = CaixaItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new CaixaHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull CaixaHolder holder, int position) {
        InvestimentoModel vale = vales.get(position);

        holder.binding.rvCaixaTvValor.setText(Util.formatarCurrency(vale.getValor()));

        holder.binding.rvCaixaTvValor.setTextColor(vale.getValor() > 0.00
                ? ctx.getResources().getColor(R.color.black)
                : ctx.getResources().getColor(R.color.gray));

        holder.binding.caixaItemCardView.setCardElevation(vale.getValor() > 0.00 ? 6f : 0f);
    }

    @Override
    public int getItemCount() {
        return vales != null ? vales.size() : 0;
    }

    public void insertItem(InvestimentoModel investimento) {
        vales.add(0, investimento);
        notifyItemInserted(0);
    }

    public static class CaixaHolder extends RecyclerView.ViewHolder {

        CaixaItemBinding binding;

        public CaixaHolder(CaixaItemBinding itemBinding) {
            super(itemBinding.getRoot());

            binding = itemBinding;

            binding.rvCaixaTvValor.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_detalhe_usuario_tv_vale, 0, 0);
        }
    }
}
