package com.zsisistemas.zsicashflow.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Window;
import android.view.WindowManager;

import androidx.core.graphics.ColorUtils;

import com.zsisistemas.zsicashflow.R;
import com.zsisistemas.zsicashflow.models.UsuarioModel;

import java.text.DecimalFormat;

public class Util {

    public static int formarCores(UsuarioModel obj) {
        int hash = obj.getNome().hashCode();
        return Color.rgb(hash, hash / 2, 0);
    }

    public static int escurecerCoresFormadas(int hash, Context ctx) {
        return ColorUtils.blendARGB(hash, ctx.getResources().getColor(R.color.black), 0.2f);
    }

    public static String formatarCurrency(Double valor) {
        DecimalFormat df = new DecimalFormat("R$###,##0.00");
        return df.format(valor).replace(",", ".");
    }

    public static void trocarCorStatusBar(Activity activityReference, int hash) {
        Window window = activityReference.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(hash);
    }
}
