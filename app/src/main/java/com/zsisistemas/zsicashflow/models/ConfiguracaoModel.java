package com.zsisistemas.zsicashflow.models;

public class ConfiguracaoModel {

    private String descricao;
    private Double valor;

    public ConfiguracaoModel() {
    }

    public ConfiguracaoModel(String descricao, Double valor) {
        this.descricao = descricao;
        this.valor = valor;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
