package com.zsisistemas.zsicashflow.models;

public class EntradaSaidaModel {

    private String data;
    private Double totalEntrada;
    private Double totalSaida;
    private Double subTotal;

    public EntradaSaidaModel() {
    }

    public EntradaSaidaModel(String data, Double totalEntrada, Double totalSaida, Double subTotal) {
        this.data = data;
        this.totalEntrada = totalEntrada;
        this.totalSaida = totalSaida;
        this.subTotal = subTotal;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Double getTotalEntrada() {
        return totalEntrada;
    }

    public void setTotalEntrada(Double totalEntrada) {
        this.totalEntrada = totalEntrada;
    }

    public Double getTotalSaida() {
        return totalSaida;
    }

    public void setTotalSaida(Double totalSaida) {
        this.totalSaida = totalSaida;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }
}
