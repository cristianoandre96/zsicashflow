package com.zsisistemas.zsicashflow.models;

public class InvestimentoModel {

    private String data;
    private String descricao;
    private Double valor;

    public InvestimentoModel() {
    }

    public InvestimentoModel(String data, String descricao, Double valor) {
        this.data = data;
        this.descricao = descricao;
        this.valor = valor;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
