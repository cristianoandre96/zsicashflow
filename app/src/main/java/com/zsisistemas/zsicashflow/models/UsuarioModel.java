package com.zsisistemas.zsicashflow.models;

import android.os.Parcel;
import android.os.Parcelable;

public class UsuarioModel implements Parcelable {

    private String nome;
    private String perfil;

    public UsuarioModel() {
    }

    public UsuarioModel(String nome, String perfil) {
        this.nome = nome;
        this.perfil = perfil;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nome);
        dest.writeString(perfil);
    }

    protected UsuarioModel(Parcel in) {
        nome = in.readString();
        perfil = in.readString();
    }

    public static final Creator<UsuarioModel> CREATOR = new Creator<UsuarioModel>() {
        @Override
        public UsuarioModel createFromParcel(Parcel in) {
            return new UsuarioModel(in);
        }

        @Override
        public UsuarioModel[] newArray(int size) {
            return new UsuarioModel[size];
        }
    };

    @Override
    public String toString() {
        return nome;
    }
}
